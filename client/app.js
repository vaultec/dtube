import './buffer';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import wakajs from 'wakajs';
import Ipfs from 'ipfs';
const IPFS = require('ipfs');

// import Gun from 'gun/gun';
// import SEA from 'gun/sea';
// import timegraph from 'gun/lib/time';
import steem from 'steem';
import AskSteem from 'asksteem';
import sc2sdk from 'sc2-sdk';
//import { session } from 'electron';
//steem.api.setOptions({ url: 'https://api.steemit.com' });
steem.api.setOptions({ url: 'https://steemd.minnowsupportproject.org/' }); 

console.log('Starting DTube APP')

FlowRouter.wait();
Meteor.startup(function(){
  process.env.ROOT_URL = document.URL;
  console.log('DTube APP Started')
  Session.set('remoteSettings', Meteor.settings.public.remote)

  if(!(UserSettings.get("steemAPI") === 0)) {
    steem.api.setOptions({ url: localStorage.getItem("steemAPI") }); //Set saved API.
  } else {
    steem.api.setOptions({ url: Meteor.settings.public.remote.APINodes[0]}); //Default
  }
  window.steem = steem

  // window.Gun = Gun

  Session.set('lastHot', null)
  Session.set('lastTrending', null)
  Session.set('lastCreated', null)
  Session.set('lastBlogs', {})
  Session.set('tagDays', 7)
  Session.set('tagSortBy', 'net_votes')
  Session.set('tagDuration', 999999)

  // load language
  loadDefaultLang(function() {
    loadLangAuto(function() {
      console.log('Loaded languages')
      // start router
      FlowRouter.initialize({hashbang: true}, function() {
        console.log('Router initialized')
      });
      // handle manual fragment change
      $(window).on('hashchange', function() {
        FlowRouter.go(window.location.hash)
      });
    })
  })


  // init steem connect
  var cbUrl
  if (window.location.hostname == 'localhost' && window.location.port == '3000')
    cbUrl = 'http://localhost:3000/#!/sc2login'
  else
    cbUrl = 'https://d.tube/#!/sc2login'
  var sc2 = sc2sdk.Initialize({
    app: 'dtube.app',
    callbackURL: cbUrl,
    accessToken: 'access_token'
  });
  window.sc2 = sc2

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  if (Session.get('remoteSettings').warning)
    toastr.warning(Session.get('remoteSettings').warning, translate('WARNING_TITLE'))

  steem.api.getDynamicGlobalProperties(function(err, result) {
    if (result)
    Session.set('steemGlobalProp', result)
  })

  Market.getSteemPrice()
  Market.getSteemDollarPrice()


  var http = new XMLHttpRequest();
  http.open("GET","https://cors-anywhere.herokuapp.com/https://gitlab.com/vaultec/dmca/raw/master/dmca.json", false);
  http.setRequestHeader("x-requested-with","https://gitlab.com/vaultec/dmca/raw/master/dmca.json");
  http.send();

  var json = JSON.parse(http.response);
  Session.set("dmca",json);

  // loading remote settings -- disabled
  // steem.api.getAccounts(['dtube'], function(err, result) {
  //   if (!result || !result[0]) return
  //   var jsonMeta = JSON.parse(result[0].json_metadata)
  //   if (jsonMeta.remoteSettings) {
  //     //Session.set('remoteSettings', jsonMeta.remoteSettings)
  //     if (jsonMeta.remoteSettings.upldr) {
  //       var rand = jsonMeta.remoteSettings.upldr[Math.floor(Math.random() * jsonMeta.remoteSettings.upldr.length)];
  //       Session.set('upldr', rand)
  //     }
  //   }
  // });


  // JS IPFS node
  // $.getScript('js/ipfs.js', function(){
  //   console.log('IPFS loaded')
  //   const repoPath = 'dtube-'+String(Math.random())
  //
  //   const node = new Ipfs({
  //     repo: repoPath,
  //     config: {
  //       Addresses: {
  //         Swarm: [
  //           '/libp2p-webrtc-star/dns4/star-signal.cloud.ipfs.team/wss'
  //         ]
  //       },
  //       Bootstrap: [
  //         "/ip4/127.0.0.1/tcp/9999/ws/ipfs/QmYRokUHWByetfpdcaaVJLrJpPtYUjXX78Ce5SSWNmFfxg"
  //       ]
  //     },
  //     init: true,
  //     start: true,
  //     EXPERIMENTAL: {
  //       pubsub: false
  //     }
  //   })
  //
  //   // expose the node to the window, for the fun!
  //   window.ipfs = node
  //
  //   node.on('ready', () => {
  //     console.log('Your node is ready to use')
  //   })
  // });
/*
if (window.ipfs && window.ipfs.enable) {
  const ipfs = await window.ipfs.enable({commands: ['files','id','dag','version']})
  console.log(await ipfs.id())
} else {
  // Fallback
}*/
  if (Meteor.settings.public.EXPERIMENTAL == true) {
    //self.addEventListener('activate', () => {
      console.log('IPFS loaded')
      const repoPath = 'dtube-ipfs';

      const node = new Ipfs({
        repo: repoPath,
        config: {
          Addresses: {
            Swarm: [
              '/dns4/wrtc-star.discovery.libp2p.io/tcp/443/wss/p2p-webrtc-star',
			        '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
            ]
          },
          /*Bootstrap: [
              //"/ip4/127.0.0.1/tcp/9999/ws/ipfs/QmYRokUHWByetfpdcaaVJLrJpPtYUjXX78Ce5SSWNmFfxg"
              "/dns4/ams-1.bootstrap.libp2p.io/tcp/443/wss/ipfs/QmSoLer265NRgSp2LA3dPaeykiS1J6DifTC88f5uVQKNAd"
            ]
         */
        },
        relay: {
          enabled: true,
          hop: {
            enabled: true
          }
        },
        init: true,
        start: true,
        EXPERIMENTAL: {
          pubsub: true
        }
      })

      // expose the node to the window, for the fun!
      window.node = node
      Session.set("ipfs", node);

      node.on('ready', () => {
        console.log('Your node is ready to use')
      })
    //})




    
  }
  if (window.ipfs && window.ipfs.enable) {
    var promise = window.ipfs.enable({commands: ['id','dag','version', 'swarm']})
    promise.then(function(value) {
      Session.set("localIpfs", value)
      window.ipfs = value;
    });
  } else {
    // Fallback
    // Setup IPFS node directly in browser
  }

  //window.localIpfs = IpfsApi(Session.get('remoteSettings').uploadNodes[Session.get('remoteSettings').uploadNodes.length-1].node)
  // setInterval(function() {
  //   try {
  //     localIpfs.repo.stat(function(e,r) {
  //       if (e) {
  //         Session.set('localIpfs', false)
  //         return;
  //       }
  //       Session.set('localIpfs', r)
  //
  //       // using local gateway seems to make my internet very unstable and nothing works
  //       // Session.set('ipfsGateway', Session.get('remoteSettings').displayNodes[Session.get('remoteSettings').displayNodes.length - 1])
  //     })
  //   } catch(e) {
  //
  //   }
  //
  // }, 10000)

})
Meteor.disconnect();
