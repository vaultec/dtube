const moment = require('moment')

TrendingTags = new Mongo.Collection(null)

TrendingTags.loadTopTags = function(limit, cb) {
  const excludedTags = [
    Meteor.settings.public.beneficiary,
    'dtube',
    '',
    'video',
    'nsfw'
    // we can add more if needed
  ]
  
  dateTo = moment().format('YYYY-MM-DD');
  dateFrom = moment().subtract(30,'d').format('YYYY-MM-DD');
  timeQ = 'created:>='+dateFrom

  var http = new XMLHttpRequest();

  http.open("POST","https://steemd.minnowsupportproject.org/", false); 
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  http.send(JSON.stringify({"id":0,"jsonrpc":"2.0","method":"call","params":["database_api","get_trending_tags",[null,25]]}));
  var results = JSON.parse(http.response).result;

 

  console.log(results);
  for (var i = 0; i < results.length; i++) {
    if (excludedTags.indexOf(results[i].name) == -1) {
      TrendingTags.upsert(results[i], results[i])
      //console.log(results[i]);
    }
  }
  

  /*AskSteem.trending({q: 'meta.video.info.title:* AND '+timeQ, types: 'tags', size: limit}, function(err, res) {
    if (err) console.log(err)
    var results = res.results
    for (var i = 0; i < results.length; i++)
      if (excludedTags.indexOf(results[i].term) == -1)
        TrendingTags.upsert(results[i], results[i])
  })*/
}