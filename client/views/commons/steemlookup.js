Template.steemlookup.sortby = ["created-DESC",
"created-ASC",
"author_reputation-DESC",
"author_reputation-ASC",
"pending_payout_value-DESC",
"pending_payout_value-ASC",
"children-DESC",
"children-ASC",
"net_votes-DESC",
"net_votes-ASC",
];

//Default query.
var basequery = {
    "page":1,
    "queryTagsInclude":"", //Insert tag
    "queryTagsExclude":"",
    "queryAuthorReputationMin":"",
    "queryAuthorReputationMax":"",
    "queryImageCountMin":"",
    "queryImageCountMax":"",
    "queryWordCountMin":"",
    "queryWordCountMax":"",
    "queryCommentCountMin":"",
    "queryCommentCountMax":"",
    "queryVoteCountMin":"",
    "queryVoteCountMax":"",
    "querySteemPowerMin":"",
    "querySteemPowerMax":"",
    "queryPendingPayoutMin":"",
    "queryPendingPayoutMax":"",
    "tagsIncludeType":"any",
    "tagsExcludeType":"any",
    "queryTitleContains":"",
    "queryBodyContains":"",
    "queryTitleNotContains":"",
    "queryBodyNotContains":"",
    "queryMinutesAgoStart":1440,
    "queryMinutesAgoEnd":0,
    "queryLanguage":"",
    "queryExcludeMackbot":false,
    "queryExcludeMyMackbot":false,
    "myMackbotList":[],
    "queryNumOfPostsToShow":"1000",
    "querySort": ""
    }

//Transfer to template for global access, learning meteor is a slow process.
Template.steemlookup.searchByTag = function(tag, Sortby) {
    if(typeof tag === "undefined") {
        tag = "";
    }
    var query = this.basequery;
    query.querySort = Sortby;
    

    

   

    return Template.steemlookup.request(query);
};
Template.steemlookup.request = function(query) {
    var http = new XMLHttpRequest();
    http.open("POST","https://steemlookup.com/query", false)
    http.setRequestHeader('Content-type', 'application/json');
    http.send(JSON.stringify(query));
    return JSON.parse(http.response);
};
Template.steemlookup.searchByTerm = function(term) {
    var query = this.basequery;
};
Template.steemlookup.getRelatedTo = function(author, permlink) {
    var json = Template.steemlookup.getContent(author, permlink);
    var json_metadata = JSON.parse(json.json_metadata);
    var query = basequery;

    console.log(json_metadata.tags);
    
    var tags = json_metadata.tags;

    query.queryTagsInclude = "dtube";

    console.log(query);
    console.log(Template.steemlookup.request(query));
    var out = Template.steemlookup.request(query);

    var results = out.results;
    var boil1 = [];
    for (var i = 0; i < results.length; i++) {
        //console.log(results[i]);
        if(results[i].tags.includes("dtube")) {
            console.log(results[i]);
            boil1.push(results[i]);
        }
    }
    console.log(boil1);
   


};
Template.steemlookup.getContent = function(author, permlink) {
    const jsonrequest = {
        method: 'get_content',
        params: [author, permlink],
        jsonrpc: "2.0",
        id: 0
    };
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    xmlhttp.open("POST", "https://steemd.minnowsupportproject.org/", false);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp.send(JSON.stringify(jsonrequest));

    var json = JSON.parse(xmlhttp.responseText).result;
    return json;
};